import React, { Component, Fragment, useState, useEffect} from "react";
import './App.css'
import CompanyList from './components/company-list'
import CompanyDetails from './components/company-details'

function App() {

  const [companies, setCompany] = useState([]);
  const [selectCompany, setSelectCompany] = useState(null);

  useEffect(() =>{
    fetch ("http://localhost:8000/api/students/", {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token'
      }
    }).then(resp => resp.json())
        .then(resp => setCompany(resp))
        .catch(error => console.log(error))
  }, []);

  const companyClicked = company => {
      console.log(company.name);
      setSelectCompany(company);
  };

  return (

      <div className="App">
        <header className="App-header">
          <h1>
            Welcome to Out Disambiguation Tool
          </h1>
        </header>

        <div className="layout">
          {/*  this {companies} comes from const [companies, setCompany]*/}
          <CompanyList companies={companies} companyClicked={companyClicked}/>
          <CompanyDetails company={selectCompany}/>
          <div>Other Column of Data</div>
        </div>
      </div>
  );
}


export default App;
