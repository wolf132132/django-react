import React from 'react'

function CompanyList(props){
    const companyClicked = company => evt => {
        props.companyClicked(company)
    };

    return (
        <div>
            Company list goes here
            {props.companies && props.companies.map( company => {
                return (
                    <div key={company.pk}>
                        <h2 onClick={companyClicked(company)}>{company.name}</h2>
                    </div>
                )
            })}
        </div>
    )
}

export default CompanyList;
