import React from 'react'

function CompanyDetails(props) {

    return (
        <div>
            { props.company ? (
                    <div>
                        <h1>{props.company.name}</h1>
                    </div>
                ) : null}
        </div>
    )
}

export default CompanyDetails;
