from django.db import models
from django.contrib.postgres.fields import ArrayField


# Create your models here.
class Company(models.Model):
    name = models.CharField("name", max_length=240, unique=True)

    def __str__(self):
        return self.name


class URL(models.Model):
    url = models.CharField("url", max_length=500)
    src = models.CharField("src", max_length=500, null=True)

    def __str__(self):
        return "%s %s" % (self.url, self.src)


class URLMapping(models.Model):
    companyid = models.ForeignKey(Company, on_delete=models.CASCADE)
    urlid = models.ForeignKey(URL, on_delete=models.CASCADE)

    def __str__(self):
        return "%s %s" % (self.companyid, self.urlid)


class Vector(models.Model):
    companyid = models.ForeignKey(Company, on_delete=models.CASCADE)
    weight = ArrayField(models.FloatField(default=0.0))

