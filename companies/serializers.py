from rest_framework import serializers
from .models import Company, URL, URLMapping


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('companyid', 'name')


class URLSerializer(serializers.ModelSerializer):
    class Meta:
        model = URL
        fields = ('urlid', 'url', 'src')


class URLMappingSerializer(serializers.ModelSerializer):
    class Meta:
        model = URLMapping
        fields = ('urlmappingid', 'companyid', 'urlid')
