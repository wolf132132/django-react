# Generated by Django 3.0.5 on 2020-09-25 01:09

from django.db import migrations
import csv

def create_data(apps, schema_editor):

    Company = apps.get_model('companies', 'Company')
    URL = apps.get_model('companies', 'URL')
    URLMapping = apps.get_model('companies', 'URLMapping')

    company_list = []
    url_list = []
    url_mapping_list = []
    with open("/Users/zirongwang/Documents/USF/cs690/django-react/input/wiki_companies_example.txt") as fd:
        rd = csv.reader(fd, delimiter="\t", quotechar='"')
        for row in rd:
            company_list.append(Company(name=row[0]))
    Company.objects.bulk_create(company_list)


    with open("/Users/zirongwang/Documents/USF/cs690/django-react/input/wiki_pages_example.txt") as fd:
        rd = csv.reader(fd, delimiter="\t", quotechar='"')
        for row in rd:
            url_list.append(URL(url=row[0]))
    URL.objects.bulk_create(url_list)

    company_objs = Company.objects.all()
    url_objs = URL.objects.all()


    with open("/Users/zirongwang/Documents/USF/cs690/django-react/input/wiki_pages_to_company_example.tsv") as fd:
        rd = csv.reader(fd, delimiter="\t", quotechar='"')
        for row in rd:
            company_name = row[1]
            url_link = row[0]
            companyid = None
            urlid = None
            for company in company_objs:
                if company.name == company_name:
                    companyid = company.id

            for url in url_objs:
                if url.url == url_link:
                    urlid = url.id

            if companyid is not None and urlid is not None:
                url_mapping_list.append(URLMapping(companyid=Company(companyid), urlid=URL(urlid)))

    URLMapping.objects.bulk_create(url_mapping_list)

    # me = User.objects.get(username='ola')


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_data),
    ]